import time
import socket
import serial
cadena=""
def aclararTexto(texto):
    texto=texto[2:len(texto)-1]
    return texto
def mandarArduino(orden):
    if orden=="Abrir" or orden=="Cerrar":
        orden=orden+"\n"
        comandoBytes=orden.encode()
        ser.write(comandoBytes)  #Mandamos a Arduino
        time.sleep(0.2)
        lectura=str(ser.readline())  #Leemos despuesta del Arduino
        lectura=lectura[2:len(lectura)-5]
        grabarEstado(lectura)  #Guardamos en el fichero logico
        return lectura
    else:  #En teoria este mensaje nunca se muestra
        return "No se ha enviado nada a Arduino: "+lectura
def grabarEstado(estado):
    global estadoPuerta
    if estado=="Abierta" or estado=="Cerrada":
        festadoPuerta = open("estadoPuerta.txt", "w") 
        time.sleep(0.1)
        festadoPuerta.write(estado)
        estadoPuerta=estado
        grabaLog("Estado cambia a "+estado)
        festadoPuerta.close()
    else:
        print("Este mensaje no deberia verse nunca-Error en grabar Estado")
def recibimosOrden():
    global cadena
    global estadoPuerta
    orden = aclararTexto(str(cli.recv(1024)))  #Recibimos la orden desde Pi1
    time.sleep(0.1)
    print ("Recibimos: "+orden)
    if orden=="Estado" or orden=="Abrir" or orden=="Cerrar":
        if orden!="Estado":
           return mandarArduino(orden) #Si el mensaje es Abrir o Cerrar
        else:
           return estadoPuerta
    elif orden.isdigit():
        cadena=cadena+orden
        print("Cadena: "+cadena)
        return "*"
    elif orden=="A":
        cadena=""
        return("Apagar")
    elif orden=="B" or orden=="C" or orden=="*" or orden=="#":
        cadena=""
        return("Limpiar")
    elif orden=="D":
        if cadena==PW:
            if estadoPuerta=="Abierta":
                orden="Cerrar"
            elif estadoPuerta=="Cerrada":
                orden="Abrir"
            cadena=""
            print("test: "+orden)
            return mandarArduino(orden)
        else:
            cadena=""
            return "Fallo En Autenticacion"
    else:
        print("Pi1 Desconectada")
        cadena=""
        reConectar()
        return ""
def enviarMensaje(mensaje):
    msg_toSend=(mensaje)
    cli.send(msg_toSend.encode('ascii'))
    print("Enviado:"+mensaje)
    grabaLog(mensaje)
def grabaLog(mensaje):
    logfile = open(".accesos2021.log", "a")  #Abrimos logfile para escribir.
    time.sleep(0.1)
    logfile.write(time.strftime("%c")+": "+mensaje+"\n")
    time.sleep(0.1)
    logfile.close()
def reConectar():
    global cli
    cli.close()
    grabaLog("No se esta recibiendo orden de Pi1, esperando reconexion. \n")
    time.sleep(5)
    sor.listen(1)  #Esperando Reconexion
    cli, addr = sor.accept()
    grabaLog("Pi1 ha retomado la conexion")
try:
    print("Esperando conexion de Pi1")
    grabaLog("Esperando conexion de Pi1")
    sor = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Declarando socket "sor"
    sor.bind(('192.168.1.132', 8040))
    sor.listen(1)
    cli, addr = sor.accept()
    festadoPuerta = open("estadoPuerta.txt")   #Cargamos del archivo logico el estado de la puerta
    time.sleep(0.1)
    estadoPuerta = str(festadoPuerta.read())
    time.sleep(0.1)
    festadoPuerta.close()
    ser = serial.Serial("/dev/ttyUSB0", baudrate=9600) #Conectamos con Arduino por USB
    print("Se ha conectado Pi1")
    cadena=""
    PW="1969"
    grabaLog("Se inicia la aplicacion con el estado: "+estadoPuerta)
    while True:
        enviarMensaje(recibimosOrden())
except KeyboardInterrupt:
    logfile = open(".accesos2021.log", "a")
    print("\nInterrupcion por teclado")
    print("Se cierra la aplicacion con la puerta "+estadoPuerta)
    logfile.write(time.strftime("%c")+": Se cierra la aplicacion con la puerta "+estadoPuerta+"\n")
    time.sleep(0.1)
    msg_toSend=("Error")
    cli.send(msg_toSend.encode('ascii'))
    time.sleep(1)
    logfile.close()
    festadoPuerta.close()
    ser.close()
    cli.close()
except ValueError as ve:
    print(ve)
    print("Otra interrupcion")
finally:
    ser.close()
    cli.close()
